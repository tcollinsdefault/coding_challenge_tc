
## OPS

Get balance
    curl -X GET "http://localhost:7000/balance/123456789/1234" -H "accept: application/json;charset=UTF-8"


Withdraw
    curl -X GET "http://localhost:7000/withdraw/123456789/1234/100" -H "accept: application/json;charset=UTF-8"
    
    
    
    
     docker run -p 7000:7000  atm_dev:1
     
     
#Details
This project consists of two directories consisting of the following
```
./
|-atmproj  (ATM  Project incluisve of build - Coding_Challenge_TC)
|-tooling  (Support tooling and scripts)
```     

### Support Tooling
The `tooling`  directory consists of number of tools that may be useful when building the app locally. 
The following tools will need to be installed and configured locally to use the following:
* docker & docker-compose

#### Sonar 
A local docker based sonar instance can be launched using the docker-compose file `docker-compose-sonar.yml`  
*Note:* Docker compose is used here merely to facilitate ease of stack startup minus the need for an orchertration platform.   
`` cd tooling &&  docker-compose -f docker-compose-sonar.yml up``

This will start a sonar instance on http://localhost:9000

#### Sonar and Jenkins Stack
The `docker-compose.yml` file will allow a Sonar and Jenkins stack to be started locally. Sonar operates the same as the above instance; Jenkins is preconfigured with some defaults but intedned as the basis for a manual testing currently. 

### ATM Building and Running 

The `atmproj` consists of a separated set of build files and a reference set of Jenkins CI files. 
The original project has been updated as follows:
* Swagger availability corrected
* Tests updated where key blockers existed, it is noted that performance is a higlighted issue on the app in the current format - the time allocated did not allow for remediation of such issues. 
* Controller service updated

The builds have been modelled to represent a separation of build and analyis/integrations via Sonar. For convienence the gradle wrapers have been comitted with the project however this is naturally non desirable in a more shared CI scenario. 
The Jenkins files capture two stages of a build cycle"
*  ``Jenkinsfile and build.gradle`` lifecycle builds inclusive of sample docker creation. This stage explicitly excludes sonar analysis
*  ``Jenkinsfile.sonar and sonar.gradle `` Sonar code analysis which also reuses coverage reports previously created in a build phase 


To build the projects the jenkins files can be loaded into a Jenkins instance or if a manual build (locally) is desired the gradle files can be launched in the standard manner. 

*Note:*  The sonar analysis defaults to http://localhost:9090 for a sonar instance, if this needs to be changed please edit ``systemProp.sonar.host.url=http://localhost:9000`` in `gradle.properties`


#### Docker file for the ATM App 
The project ships with a sample docker file.  The default Jenkins build will create a local docker image using the environment name to form the tag value. 
After the image has been built, it can be launched / checked as follows (for a sample env named dev):
```
 docker run -p 7000:7000  atm_dev:1
```

   
### Misc
* For reference a set of ``jacoco`` reports and a sonar report output are also left intact in the build folder. 
